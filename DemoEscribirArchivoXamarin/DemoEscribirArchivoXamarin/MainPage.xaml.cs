﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DemoEscribirArchivoXamarin
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void CrearArchivo()
        {
            string baseDireccion = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string elementos = "";
            for (int i = 0; i < 120; i++)
            {
                elementos += $"{i},";
            }
            baseDireccion = Path.Combine(baseDireccion, $"{Guid.NewGuid().ToString()}.csv");
            File.WriteAllText(baseDireccion, elementos);
            if(File.Exists(baseDireccion))
            {
                DisplayAlert("Demo", "Existe el archivo", "OK");
            }
            else
            {
                DisplayAlert("Demo", "No existe el archivo", "OK");
            }

            List<string> archivos = Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.Personal)).ToList();

            //Process.Start(baseDireccion);
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            CrearArchivo();
        }
    }
}
